
# 'nlohmann_json' JSON Parser Example

This package shows you how to use the `nlohmann_json` JSON parser, which is part of the ATLAS Externals.

## 1 - Compile the `nlohmann_json` library

First, compile the `nlohmann_json` package from the ATLAS Externals:

```bash
git clone https://:@gitlab.cern.ch:8443/atlas/atlasexternals.git
mkdir build_ax
cd build_ax
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCTEST_USE_LAUNCHERS=TRUE ../atlasexternals/Projects/AthenaExternals/
make Package_nlohmann_json
cd ../ # leave the "build_ax" folder
```

## 2 - Compile the test package

First, copy the test folders and copy the template files, to get proper source files:

```
cp -r atlasexternals/External/nlohmann_json/example test_nlohmann_json
cd test_nlohmann_json
cp CMakeLists.txt.template CMakeLists.txt
cp main.cxx.template main.cxx
cd ../ # leave the "test_nlohmann_json" folder
```

Then, create a build directory, setup the path of the `nlohmann_json` we just compiled (**IMPORTANT!**), and compile the test package:

```bash
mkdir build_test
cd build_test
source ../build_ax/x86_64-slc6-gcc62-opt/setup.sh # this let us use the "nlohmann_json" package we have just compiled
cmake ../test_nlohmann_json/
make
```

## 3 - Test

Then, you can test the package by running the output executable:

```
./test_nlohmann_json_exe
```

HAPPY CODING! :)


