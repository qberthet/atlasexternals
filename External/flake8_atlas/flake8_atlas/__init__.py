# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

# Version of the package:
__version__ = '2.0.0'
