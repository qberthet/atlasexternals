# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Register flake8 plugins:
#   http://flake8.pycqa.org/en/latest/plugin-development/registering-plugins.html
#
import setuptools
from flake8_atlas import __version__

requires = [
   "flake8 >= 6.0.0",
]

setuptools.setup(
   name="flake8_atlas",
   version=__version__,
   description="ATLAS plugins for flake8",
   author='Frank Winklmeier',
   url='https://gitlab.cern.ch/atlas/atlasexternals/tree/main/External/flake8_atlas',
   test_loader='unittest:TestLoader',
   packages=['flake8_atlas'],
   zip_safe=True,
   entry_points={
      'flake8.extension': [
         'ATL100 = flake8_atlas.checks:delayed_string_interpolation',
         'ATL900 = flake8_atlas.checks:OutputLevel',
         'ATL901 = flake8_atlas.checks:print_for_logging',
         'ATL902 = flake8_atlas.checks:Copyright'
      ]
   }
)
