ONNX Runtime
============

This package builds the
[ONNX Runtime](https://microsoft.github.io/onnxruntime/) library using
the [source code from GitHub](https://github.com/microsoft/onnxruntime).

Note that the build makes use of a large number of "externals", which are
packaged together with the ONNX Runtime sources. This should not cause an
issue during linking however, as `libonnxruntime.so` is **very** explicit
about which symbols it would export. So the fact that internally it uses
a different version of let's say [Eigen](http://eigen.tuxfamily.org) than
what the rest of the ATLAS code uses, should not be a problem.
