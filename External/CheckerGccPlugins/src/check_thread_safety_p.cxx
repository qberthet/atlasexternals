/**
 * @file CheckerGccPlugins/src/check_thread_safety_p.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2015
 * @brief Test to see if something should be checked for thread-safety.
 *
 * check_thread_safety_p(decl) returns false if DECL has a not_thread_safe
 * attribute.  Otherwise, it returns true in the following cases:
 *
 *  - DECL directly has a check_thread_safety attribute.
 *  - DECL is a function or type and a containing context has thread-safety
 *    checking on.
 *  - The file contains #pragma ATLAS check_thread_safety.
 *  - The package contains a file ATLAS_CHECK_THREAD_SAFETY.
 *
 * If a decl has the attribute check_thread_safety_debug, then a diagnostic
 * will be printed saying if that decl has thread-safety checking enabled.
 *
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
 */


#include <unordered_set>
#include <unordered_map>
#include <map>
#include <set>
#include <string>
#include "checker_gccplugins.h"
#include "tree.h"
#include "cp/cp-tree.h"
#include "diagnostic.h"
#include "print-tree.h"
#include "stringpool.h"
#include "attribs.h"
#include "input.h"


#if GCC_VERSION < 12000
using CheckerGccPlugins::startswith;
#endif


//**************************************************************************
// Callbacks for thread-safety check debugging.
//

namespace {


// If requested, emit a message saying whether the given decl
// has been marked as thread-safe.
std::unordered_set<location_t> seen_loc;
void thread_safe_debug_finishdecl_callback (void* gcc_data, void* /*user_data*/)
{
  tree decl = (tree)gcc_data;
  if (!DECL_P (decl)) return;
  location_t loc = DECL_SOURCE_LOCATION (decl);
  if (!loc) return;
  if (!seen_loc.insert (loc).second)
    return;

  tree attribs = DECL_ATTRIBUTES (decl);
  if (attribs == NULL_TREE && TREE_CODE (decl) == TYPE_DECL)
    attribs = TYPE_ATTRIBUTES (TREE_TYPE (decl));
  if (lookup_attribute ("check_thread_safety_debug", attribs))
  {
    const char* flag = " not";
    if (CheckerGccPlugins::check_thread_safety_p (decl))
      flag = "";
    warning_at (DECL_SOURCE_LOCATION (decl), 0,
                "%<%D%> is%s marked thread-safe", decl, flag);
    
  }
}


void thread_safe_debug_pregen_callback (void* gcc_data, void* user_data)
{
  tree ast = (tree)gcc_data;
  if (TREE_CODE (ast) == FUNCTION_DECL)
    thread_safe_debug_finishdecl_callback (gcc_data, user_data);
}


} // anonymous namespace


//**************************************************************************
// Helpers for figuring out which files to check.
//


namespace {


// Remember if a given file is considered thread-safe.
typedef std::unordered_map<std::string, bool> thread_safe_files_t;
thread_safe_files_t thread_safe_files;


// List of directories at which we stop the upward walk for an
// ATLAS_CHECK_THREAD_SAFETY file.
const std::unordered_set<std::string> stop_walk_dirs
{
    "InstallArea",

    "AtlasGeometryCommon",
    "AtlasTest",
    "Calorimeter",
    "Commission",
    "Control",
    "Database",
    "DataQuality",
    "DetectorDescription",
    "Event",
    "External",
    "ForwardDetectors",
    "Generators",
    "graphics",
    "HighGranularityTimingDetector",
    "HLT",
    "InnerDetector",
    "LArCalorimeter",
    "LumiBlock",
    "MagneticField",
    "MuonSpectrometer",
    "PhysicsAnalysis",
    "Reconstruction",
    "Simulation",
    "TestBeam",
    "TileCalorimeter",
    "Tools",
    "Tracking",
    "Trigger",
    "graphics",

    "boost",
    "root",
    "Eigen",
    "fastjet",
    "clhep",
    "rootsys",
    "ROOT",
    "gcc",
    "usr",
    };


//
// Return the first component of PATH.
// I.e., if PATH is /A/B/C, return A.
//
std::string topdir (const std::string& path)
{
  if (path.size() < 2) return "";
  std::string::size_type pos0 = 0;
  if (path[pos0] == '/') {
    pos0 += 1;
  }
  std::string::size_type pos = path.find ('/', pos0);
  if (pos != std::string::npos) {
    return path.substr (pos0, pos-pos0);
  }
  return path.substr (pos0, path.size() - pos0);
}


//
// Read the list of paths to check from the config file.
// This is given by the thread.check_paths config key.
// All packages under a given path are checked, unless the path starts with -,
// in which case those paths are excluded from checking.
// If there are multiple matches, longest one wins.
// Returns a map of paths to a flag saying if we should check under
// that path.
// Paths are normalized so that they both begin and end with a slash.
//
std::map<std::string, bool> read_thread_safety_check_list()
{
  std::map<std::string, bool> paths;
  for (const std::string& s : CheckerGccPlugins::config["thread.check_paths"]) {
    std::string dir = s;
    if (dir.empty()) continue;

    // Handle leading -
    bool flag = true;
    if (dir[0] == '-') {
      flag = false;
      dir.erase (0, 1);
      if (dir.empty()) continue;
    }

    //  Make sure the path begins and ends with slashes.
    if (dir[0] != '/') {
      dir.insert (0, "/");
    }
    if (dir.back() != '/') {
      dir += '/';
    }

    paths[dir] = flag;
  }
    
  if (!CheckerGccPlugins::config["dump"].empty())
  {
    std::cout << "thread-safety checker paths\n";
    for (const auto& p : paths) {
      std::cout << "  " << p.first << " " << p.second << "\n";
    }
  }
  return paths;
}


//
// Take the list of paths previously read from the config and construct
// the set of topmost directories from those paths.
//
std::set<std::string> find_topdirs (const std::map<std::string, bool>& paths)
{
  std::set<std::string> topdirs;
  for (const auto& p : paths) {
    topdirs.insert (topdir (p.first));
  }

  if (!CheckerGccPlugins::config["dump"].empty())
  {
    std::cout << "thread-safety checker topdirs\n";
    for (const auto& p : topdirs) {
      std::cout << "  " << p << "\n";
    }
  }
  return topdirs;
}


//
// Given a path DIR containing source files we may want to check,
// try to match it against the list of topmost directories we found
// from the config file.  If found, returns a pair of the position in DIR
// and the topmost directory name.
//
std::pair<std::string::size_type, std::string>
find_topdir (const std::string& dir, std::set<std::string> topdirs)
{
  for (const std::string& s : topdirs) {
    std::string topdir = "/" + s + "/";
    std::string::size_type pos = dir.find (topdir);
    if (pos != std::string::npos) {
      return std::make_pair (pos, topdir);
    }
  }
  return std::make_pair (std::string::npos, "");
}


//
// Check a directory IDIR and see if the config file tells us if we
// should or should not do thread-safety checking for files in this directory.
// First flag in the return tells if we matched the directory in the config
// file.  If that's true, the second flag tells if the directory is to be
// checked.
//
std::pair<bool, bool> is_thread_safe_from_config (const std::string& idir)
{
  // Read configuration.
  static const std::map<std::string, bool> config_paths =
    read_thread_safety_check_list();
  static const std::set<std::string> topdirs =
    find_topdirs (config_paths);

  // Make sure directory starts and ends with slashes.
  std::string dir = idir;
  if (dir.size() < 2) {
    return std::make_pair (false, false);
  }
  if (dir[0] != '/') {
    dir.insert (0, "/");
  }
  if (dir.back() != '/') {
    dir += '/';
  }

  // Try to match against the list of topmost directories from the
  // config file.  If we find one, that gives us the spot in the path
  // that represents the root of the release.  Ignore anything before that.
  auto [pos, topdir] = find_topdir (dir, topdirs);
  if (pos == std::string::npos) {
    return std::make_pair (false, false);
  }
  dir.erase (0, pos);

  // Search the list of paths from the config.
  // Handle an exact match here.
  auto it = config_paths.lower_bound (dir);
  if (it != config_paths.end() && it->first == dir) {
    return std::make_pair (true, it->second);
  }

  // Otherwise search backwards until we get a match.  This should end up
  // matching the longest one.  Give up when we hit a different
  // top directory.
  while (it != config_paths.begin()) {
    --it;
    if (!startswith (it->first.c_str(), topdir.c_str())) break;
    if (startswith (dir.c_str(), it->first.c_str())) {
      return std::make_pair (true, it->second);
    }
  }

  return std::make_pair (false, false);
}


} // anonymous namespace


namespace CheckerGccPlugins {


bool is_thread_safe_dir (const std::string& dir, int height = 0);
bool is_thread_safe_dir1 (std::string dir, int height = 0)
{
  const std::string flagname = "/ATLAS_CHECK_THREAD_SAFETY";
  std::string flagfile = dir + flagname;
  if (access (flagfile.c_str(), R_OK) == 0) {
    return true;
  }

  while (endswith (dir.c_str(), "/..")) {
    dir.erase (dir.size()-3, 3);
    std::string::size_type dpos = dir.rfind ('/');
    if (dpos != std::string::npos) {
      dir.erase (dpos, std::string::npos);
    }
  }

  std::string::size_type dpos = dir.rfind ('/');
  std::string dir2 = "..";
  std::string dname = dir;
  if (dpos != std::string::npos) {
    dir2 = dir.substr (0, dpos);
    dname = dir.substr (dpos+1, std::string::npos);
  }

  std::string cmakefile = dir + "/CMakeLists.txt";
  if (access (cmakefile.c_str(), R_OK) == 0) {
    // Check for a flag file in an include dir.
    flagfile = dir + "/" + dname + flagname;
    if (access (flagfile.c_str(), R_OK) == 0) {
      return true;
    }

    return false;
  }

  if (height > 5 || stop_walk_dirs.count (dname) > 0)
    return false;

  if (dir2.empty())
    return false;

  if (dname == "..")
    dir2 += "/../..";

  return is_thread_safe_dir (dir2, height+1);
}


bool is_thread_safe_dir (const std::string& dir, int height /*= 0*/)
{
  thread_safe_files_t::iterator it = thread_safe_files.find (dir);
  if (it != thread_safe_files.end()) return it->second;

  // First check the config file.  If we find a match, then skip
  // the directory walk.
  if (height == 0) {
    auto [found, ret] = is_thread_safe_from_config (dir);
    if (found) {
      thread_safe_files[dir] = ret;
      return ret;
    }
  }

  bool ret = is_thread_safe_dir1 (dir, height);
  thread_safe_files[dir] = ret;
  return ret;
}


bool check_thread_safety_location_p (location_t loc)
{
  // Never check code in headers found via -isystem.
  if (in_system_header_at (loc)) {
    return false;
  }

  std::string file = LOCATION_FILE(loc);
  thread_safe_files_t::iterator it = thread_safe_files.find (file);
  if (it != thread_safe_files.end()) return it->second;

  std::string::size_type dpos = file.rfind ('/');
  std::string dir = ".";
  if (dpos != std::string::npos)
    dir = file.substr (0, dpos);

  bool ret = is_thread_safe_dir (dir);
  thread_safe_files[file] = ret;
  return ret;
}


/// Has DECL been declared for thread-safety checking?
bool check_thread_safety_p (tree decl)
{
  // Shut off checking entirely if we're processing a root dictionary.
  static enum { UNCHECKED, OK, SKIP } checkedMain = UNCHECKED;
  if (checkedMain == UNCHECKED) {
    if (strstr (main_input_filename, "ReflexDict.cxx") != 0) {
      checkedMain = SKIP;
    }
    else if (strstr (main_input_filename, "CintDict.cxx") != 0) {
      checkedMain = SKIP;
    }
    else if (strstr (main_input_filename, "_gen.cxx") != 0) {
      checkedMain = SKIP;
    }
    else if (strstr (main_input_filename, "_rootcint.cxx") != 0) {
      checkedMain = SKIP;
    }
    else {
      checkedMain = OK;
    }
  }
  if (checkedMain == SKIP) {
    return false;
  }

  tree attribs = DECL_ATTRIBUTES (decl);

  // For a TYPE_DECL, look at the type attributes.
  // Also, while one can declare attributes for a lambda, they attach
  // to the _type_, not the function.
  if (attribs == NULL_TREE && (LAMBDA_FUNCTION_P(decl) ||
                               TREE_CODE (decl) == TYPE_DECL))
  {
    attribs = TYPE_ATTRIBUTES (TREE_TYPE (decl));
  }

  // A similar deal for constructors and destructors.
  else if (attribs == NULL_TREE && (DECL_CXX_DESTRUCTOR_P (decl) ||
                                    DECL_CXX_CONSTRUCTOR_P (decl)))
   {
    attribs = TYPE_ATTRIBUTES (TREE_TYPE (decl));
  }

  // Check if attributes are present directly.
  if (lookup_attribute ("not_thread_safe", attribs))
    return false;
  if (lookup_attribute ("check_thread_safety", attribs))
    return true;

  // If it's a function or class, check the containing function or class.
  if (TREE_CODE (decl) == FUNCTION_DECL  ||
      TREE_CODE (decl) == TYPE_DECL)
  {
    tree ctx = DECL_CONTEXT (decl);
    while (ctx && !SCOPE_FILE_SCOPE_P (ctx)) {
      if (TREE_CODE (ctx) == RECORD_TYPE) {
        if (lookup_attribute ("check_thread_safety", TYPE_ATTRIBUTES (ctx)))
          return true;
        if (lookup_attribute ("not_thread_safe", TYPE_ATTRIBUTES (ctx)))
          return false;
        if (check_thread_safety_location_p (DECL_SOURCE_LOCATION (TYPE_NAME (ctx))))
          return true;
        ctx = TYPE_CONTEXT (ctx);
      }

      else if (TREE_CODE (ctx) == FUNCTION_DECL) {
        if (lookup_attribute ("check_thread_safety", DECL_ATTRIBUTES (ctx)))
          return true;
        if (check_thread_safety_location_p (DECL_SOURCE_LOCATION (ctx)))
          return true;
        ctx = DECL_CONTEXT (ctx);
      }

      else
        break;
    }
  }

  // Check the file in which it was declared.
  if (check_thread_safety_location_p (DECL_SOURCE_LOCATION (decl)))
    return true;

  return false;
}


void handle_check_thread_safety_pragma (cpp_reader*)
{
  thread_safe_files[LOCATION_FILE (input_location)] = true;
}


void handle_no_check_thread_safety_pragma (cpp_reader*)
{
  thread_safe_files[LOCATION_FILE (input_location)] = false;
}


} // namespace CheckerGccPlugins


void init_thread_safe_debug_checker (plugin_name_args* plugin_info)
{
  register_callback (plugin_info->base_name,
                     PLUGIN_FINISH_DECL,
                     thread_safe_debug_finishdecl_callback,
                     NULL);
  register_callback (plugin_info->base_name,
                     PLUGIN_PRE_GENERICIZE,
                     thread_safe_debug_pregen_callback,
                     NULL);
}
