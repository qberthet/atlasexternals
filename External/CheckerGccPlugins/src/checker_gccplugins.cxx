/**
 * @file CheckerGccPlugins/src/checker_gccplugins.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2014
 * @brief Framework for running checker plugins in gcc.
 *
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
 */


#include <string>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <stdlib.h>
#include "checker_gccplugins.h"

#include "plugin-version.h"
#include "options.h"
#include "tree-core.h"
#include "tree-pass.h"
#include "plugin.h"
#include "c-family/c-pragma.h"
#include "tree.h"
#include "cp/cp-tree.h"
#include "diagnostic.h"
#include "gimple.h"


/* Declare GPL compatible license. */
int plugin_is_GPL_compatible;


/// Table of defined checkers.  All on by default.
struct Checker
{
  std::string name;
  bool enabled;
  void (*initfunc) (plugin_name_args*);
};

static Checker checkers[] = {
#define CHECKER(NAME, FLAG) { #NAME, FLAG, init_##NAME##_checker },
#include "checkers.def"
#undef CHECKER
};


extern "C" {


static
void print_plugin_version(struct plugin_name_args *plugin_info)
{
  printf("ATLAS gcc checker plugins info:\n"
         "\tVersion:  %s\n"
         "\tbase_name: %s\n"
         "\tfull_name: %s\n",
          plugin_info->version,
          plugin_info->base_name,
          plugin_info->full_name);

  printf("GCC info:\n"
         "\tVersion: %s\n",
          gcc_version.basever);
}


static
bool handle_checker_arg (const std::string& arg)
{
  if (arg == "all") {
    for (Checker& c : checkers)
      c.enabled = true;
    return true;
  }

  if (arg == "none") {
    for (Checker& c : checkers)
      c.enabled = false;
    return true;
  }

  for (Checker& c : checkers) {
    if (arg == c.name) {
      c.enabled = true;
      return true;
    }
    else if (arg == "no-" + c.name) {
      c.enabled = false;
      return true;
    }
  }

  return false;
}


/**
 * Collect command-line arguments from `plugin_init(..)`
 */
static
int
collect_plugin_args (plugin_name_args *plugin_info)
{
  for (int i=0; i < plugin_info->argc; i++)
  {
    plugin_argument& arg = plugin_info->argv[i];
    std::string key = arg.key;
    std::string val_orig = arg.value ? arg.value : "";

    bool bad = false;

    if (key == "version")
      print_plugin_version (plugin_info);

    else if (key == "config" && arg.value) {
      char* ptr = nullptr;
      char* str = arg.value;
      while (char* val = strtok_r (str, ":", &ptr)) {
        str = 0;
        CheckerGccPlugins::config.read (val);
      }
    }

    else if (key == "checkers" && arg.value) {
      char* ptr;
      char* str = arg.value;
      while (char* val = strtok_r (str, ",", &ptr)) {
        str = 0;
        if (!handle_checker_arg (val)) {
          bad = true;
          break;
        }
      }
    }

    else
      bad = true;

    if (bad) {
      // Print a pretty error message, showing in bold where the option
      // went wrong.
      std::cerr << "Unrecognised plugin argument: " << key;
      if (arg.value)
        std::cerr << "=" << val_orig;
      std::cerr << "\n";
      std::cerr << "Allowed arguments:\n";
      std::cerr << "\t-fplugin-arg-" << plugin_info->base_name << "-version\n";
      std::cerr << "\t-fplugin-arg-" << plugin_info->base_name << "-config=CONFIG\n";
      std::cerr << "\t  CONFIGS is a colon-separated list configuration files.\n";
      std::cerr << "\t-fplugin-arg-" << plugin_info->base_name << "-checkers=LIST\n";
      std::cerr << "\n";
      std::cerr << "\tLIST is a comma-separated list of checker names.\n";
      std::cerr << "\tPrefix a name with `no-' to disable it; use `all' or `none'\n";
      std::cerr << "\tto enable/disable all checkers.|n";
      std::cerr << "\n";
      std::cerr << "Available checkers:\n";
      for (Checker& c : checkers)
        std::cerr << "\t" << c.name << "\n";
      return 1;
    }
  }

  return 0;
}

// Plugin info Structs
static struct plugin_info
checker_plugin_info = {
  /*.version =*/ CHECKER_GCCPLUGINS_VERSION_FULL,
  /*.help =*/ "Collection of ATLAS checker plugins for gcc."
};


# define ATTRIB(A) {A, 0, 0, false, false, false, false, nullptr, nullptr}
static struct
attribute_spec attribs[] =
                     
  { ATTRIB("thread_safe"),
    ATTRIB("not_thread_safe"),
    ATTRIB("not_reentrant"),
    ATTRIB("not_const_thread_safe"),
    ATTRIB("argument_not_const_thread_safe"),
    ATTRIB("check_thread_safety"),
    ATTRIB("check_thread_safety_debug"),
    ATTRIB(nullptr),
  };
#undef ATTRIB


static
void
register_checker_attributes (void* /*event_data*/, void* /*data*/)
{
  //fprintf (stderr, "register_attributes %s\n", thread_safe_attr.name);
  register_scoped_attributes (attribs, "ATLAS"
#if GCC_VERSION >= 12000
                              , false
#endif
                              );
}


static
void
register_checker_pragmas (void* /*event_data*/, void* /*data*/)
{
  c_register_pragma ("ATLAS", "check_thread_safety",
                     CheckerGccPlugins::handle_check_thread_safety_pragma);
  c_register_pragma ("ATLAS", "no_check_thread_safety",
                     CheckerGccPlugins::handle_no_check_thread_safety_pragma);

  cpp_define (parse_in, "ATLAS_GCC_CHECKERS");
}


int
plugin_init(struct plugin_name_args   *plugin_info,
            struct plugin_gcc_version *version)
{
  plugin_info->version = CHECKER_GCCPLUGINS_VERSION_FULL;


  // Complain if built for a different major, or minor version.
  // Ignore patchlevel and other `plugin_gcc_version` attributes.
  if (strncmp(version->basever, gcc_version.basever, 3))
  {
    fprintf(stderr, "Incompatible version (%s). Compiled for %s\n",
        version->basever, gcc_version.basever);
    return 1;
  }

  // Handle command-line arguments.
  if (collect_plugin_args(plugin_info)!=0)
    return 1;

  // Read any additional config files specified via the environment.
  CheckerGccPlugins::config.readViaEnv ("CHECKERGCCPLUGINS_CONFIG");

  // Dump configuration info if requested.
  if (!CheckerGccPlugins::config["dump"].empty()) {
    CheckerGccPlugins::config.dump (std::cout);
  }

  /* Populate the plugin_info struct */
  register_callback(plugin_info->base_name, PLUGIN_INFO,
                    NULL, &checker_plugin_info);

  for (Checker& c : checkers) {
    if (c.enabled) c.initfunc (plugin_info);
  }

  // Register callback to set up common attributes and pragmas.
  register_callback (plugin_info->base_name,
                     PLUGIN_ATTRIBUTES,
                     register_checker_attributes,
                     NULL);
  register_callback (plugin_info->base_name,
                     PLUGIN_PRAGMAS,
                     register_checker_pragmas,
                     NULL);

  return 0;
}


} // extern "C"



namespace CheckerGccPlugins {


// Configuration info.
CheckerConfig config;



void inform_url (location_t loc, const char* url)
{
  if (!in_system_header_at(loc) && !global_dc->dc_warn_system_headers)
    inform (loc, "See %s.", url);
}


/// If STMT represents a virtual call, return the fndecl of the called function.
tree vcall_fndecl (gimplePtr stmt)
{
  if (gimple_call_fndecl (stmt)) return NULL_TREE;
  tree fn = gimple_call_fn (stmt);
  if (!fn || TREE_CODE (fn) != OBJ_TYPE_REF) return NULL_TREE;
  tree obj = OBJ_TYPE_REF_OBJECT (fn);
  tree ndx_tree = OBJ_TYPE_REF_TOKEN (fn);
  int ndx = TREE_INT_CST_LOW (ndx_tree);

  if (TREE_CODE (obj) != SSA_NAME) return NULL_TREE;
  tree typ = TREE_TYPE (obj);
  if (POINTER_TYPE_P (typ))
    typ = TREE_TYPE (typ);

  if (TREE_CODE (typ) == VOID_TYPE) {
    // We can get here if we have something like
    //  const void* p = ...;
    //  delete reinterpret_cast<const T*(p);
    // The type information gets lost in the call.
    // In this case, we need to trace back through where the vtbl pointer is found.
    tree expr = OBJ_TYPE_REF_EXPR (fn);
    if (TREE_CODE (expr) == SSA_NAME) {
      gimplePtr stmt2 = SSA_NAME_DEF_STMT (expr);
      if (is_gimple_assign (stmt2) && gimple_expr_code (stmt2) == MEM_REF) {
        tree val = TREE_OPERAND (gimple_op (stmt2, 1), 0);
        if (TREE_CODE (val) == SSA_NAME) {
          gimplePtr stmt3 = SSA_NAME_DEF_STMT (val);
          if (is_gimple_assign (stmt3) && gimple_expr_code (stmt3) == POINTER_PLUS_EXPR) {
            tree val3 = gimple_op (stmt3, 1);
            if (TREE_CODE (val3) == SSA_NAME) {
              gimplePtr stmt4 = SSA_NAME_DEF_STMT (val3);
              if (is_gimple_assign (stmt4) && gimple_expr_code (stmt4) == COMPONENT_REF) {
                tree val4 = TREE_OPERAND (gimple_op (stmt4, 1), 0);
                if (TREE_CODE (val4) == MEM_REF || TREE_CODE (val4) == COMPONENT_REF) {
                  typ = TREE_TYPE (val4);
                }
              }
            }
          }
        }
      }
    }
  }

  if (TREE_CODE (typ) != RECORD_TYPE) return NULL_TREE;

  vec<tree, va_gc>* method_vec = CLASSTYPE_MEMBER_VEC (typ);
  tree p;
  unsigned ix;
  FOR_EACH_VEC_SAFE_ELT (method_vec, ix, p) {
    for (ovl_iterator it (p); it; ++it) {
      if (*it && TREE_CODE (*it) == FUNCTION_DECL &&
          DECL_VIRTUAL_P (*it) &&
          // We may also get entries from virtual bases in method_vec.
          // These will have their own vindex sequence, so they may
          // collide with what we're looking for.  Just ignore such entries.
          // But careful: Can't just compare the types, because TYP
          // may be a const type.
          TYPE_NAME (strip_typedefs (typ)) == TYPE_NAME (strip_typedefs (CP_DECL_CONTEXT (*it)))
          )
      {
        tree fn_ndx_tree = DECL_VINDEX (*it);
        if (fn_ndx_tree && TREE_CODE (fn_ndx_tree) == INTEGER_CST) {
          int ndx_tree = TREE_INT_CST_LOW (fn_ndx_tree);
          if (ndx_tree == ndx) {
            return *it;
          }
        }
      }
    }
  }
  return NULL_TREE;
}


// CTX should be either a type or a decl, representing a scope.
// Return a string giving a printable representation of the scope,
// like `A::B::C::'.  Template arguments are not included.
std::string context_string (tree ctx)
{
  if (ctx == global_namespace) {
    return "";
  }
  tree ctx2;
  tree id;
  if (DECL_P (ctx)) {
    id = DECL_NAME (ctx);
    ctx2 = CP_DECL_CONTEXT (ctx);
  }
  else {
    id = TYPE_IDENTIFIER (ctx);
    ctx2 = CP_TYPE_CONTEXT (ctx);
  }
  return context_string (ctx2) + (id ? IDENTIFIER_POINTER (id) : "") + "::";
}


// Return FNDECL as a string, including the scope, omitting
// function/template arguments.
std::string fndecl_string (tree fndecl)
{
  tree id = DECL_NAME (fndecl);
  if (!id) return "";
  return context_string (CP_DECL_CONTEXT (fndecl)) + IDENTIFIER_POINTER (id);
}


} // namespace CheckerGccPlugins

