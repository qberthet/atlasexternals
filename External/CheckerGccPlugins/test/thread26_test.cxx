// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
// thread26_test: testing a crash seen in check_nonconst_pointer_member_passing


#pragma ATLAS check_thread_safety;


namespace std
{

template<typename _Tp, typename... _Args>
void make_unique(_Args&&... __args);

} // namespace



template <class T> class ReadHandleKey {};

using Payload = ReadHandleKey<int>;

class CacheGlobalMemory
{
public:
  void assert_decision() const;
  Payload* m_ptr;
};


struct DUS
{
  DUS(Payload*);
};


void CacheGlobalMemory::assert_decision() const {
  std::make_unique<DUS>(m_ptr);
}
