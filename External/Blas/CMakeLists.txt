# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration file building the BLAS library as part of the project
# build.
#

# The name of the package:
atlas_subdir( Blas )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get BLAS from.
set( ATLAS_BLAS_SOURCE
   "URL;${CMAKE_CURRENT_SOURCE_DIR}/src/blas-20070405.tar.gz;URL_MD5;64428bb36a41ab470039354eeaaa1472"
   CACHE STRING "The source for BLAS" )
mark_as_advanced( ATLAS_BLAS_SOURCE )

# Decide whether / how to patch the BLAS sources.
set( ATLAS_BLAS_PATCH "" CACHE STRING "Patch command for BLAS" )
set( ATLAS_BLAS_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of BLAS (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_BLAS_PATCH ATLAS_BLAS_FORCEDOWNLOAD_MESSAGE )

# Extend the CMAKE_Fortran_FLAGS variable with the build type specific
# flags, as we only use that variable in setting up the make.inc file.
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   string( TOUPPER ${CMAKE_BUILD_TYPE} _type )
   set( CMAKE_Fortran_FLAGS
      "${CMAKE_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_${_type}}" )
   unset( _type )
endif()

# Configure BLAS's makefile:
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/src/make.inc.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc" @ONLY )

# Build BLAS for the build area:
ExternalProject_Add( Blas
   PREFIX ${CMAKE_BINARY_DIR}
   ${ATLAS_BLAS_SOURCE}
   ${ATLAS_BLAS_PATCH}
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E copy
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc" "make.inc"
   BUILD_IN_SOURCE 1
   INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory
   "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}"
   COMMAND ${CMAKE_COMMAND} -E copy "libblas.a"
   "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/" )
ExternalProject_Add_Step( Blas forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_BLAS_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
add_dependencies( Package_Blas Blas )

# Install BLAS:
install( FILES "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/libblas.a"
   DESTINATION "${CMAKE_INSTALL_LIBDIR}" OPTIONAL )
